(ns kaddy-clj.core
  (:gen-class)
  (:import (net.dv8tion.jda.core JDABuilder AccountType))
  (:import (net.dv8tion.jda.core.hooks ListenerAdapter)
           (java.io StringWriter)
           (java.util.concurrent ExecutionException))
  (:require [clojure.string :as str]
            [owner-sandbox])
  (:use [clojail.core :only [sandbox safe-read]]
        [clojail.testers :only [secure-tester
                                secure-tester-without-def
                                blacklist-symbols
                                blacklist-objects
                                blacklist-packages
                                blacklist-nses
                                blanket]]))

(def owner-tester
  [(blacklist-objects [clojure.lang.Compiler clojure.lang.Ref clojure.lang.Reflector
                       clojure.lang.Namespace clojure.lang.Var clojure.lang.RT
                       java.io.ObjectInputStream])
   (blacklist-packages ["java.lang.reflect"
                        "java.security"
                        "java.util.concurrent"
                        "java.awt"])
   (blacklist-symbols
     '#{alter-var-root intern eval catch
        load-string load-reader addMethod ns-resolve resolve find-var
        *read-eval* ns-publics set! ns-map ns-interns the-ns
        push-thread-bindings pop-thread-bindings future-call agent send
        send-off pmap pcalls pvals in-ns System/out System/in System/err
        with-redefs-fn Class/forName})
   (blacklist-nses '[clojure.main kaddy-clj.core])
   (blanket "clojail")])

(defonce securer-tester
         (conj (conj secure-tester-without-def
                     (blacklist-symbols '#{def}))
               (blacklist-nses '[owner-sandbox kaddy-clj.core])))

(defn make-safe-eval-with-sandbox [our-sandbox]
  (let [history (atom [nil nil nil])
        last-exception (atom nil)]
    (fn [form output]
      (binding [*out* output]
        (try
          (let [bindings {#'*print-length* 30
                          #'*1 (nth @history 0)
                          #'*2 (nth @history 1)
                          #'*3 (nth @history 2)
                          #'*e @last-exception
                          #'*out* output
                          #'*err* output}
                result (our-sandbox form bindings)]
            (swap! history (constantly [result (nth @history 0) (nth @history 1)]))
            (pr result))
          (catch ExecutionException e
            (swap! last-exception (constantly (.getCause e)))
            (print (.getMessage e)))
          (catch Throwable t
            (swap! last-exception (constantly t))
            (print (.getMessage t))))))))

(defn make-safe-eval [jda owner]
  (create-ns 'owner-sandbox)
  (intern 'owner-sandbox 'jda jda)
  (let [our-sandbox (if owner (sandbox owner-tester :namespace 'owner-sandbox) (sandbox securer-tester))]
    (make-safe-eval-with-sandbox our-sandbox)))

(defn handle-message
  [safe-eval message]
  (let [translated-message (str/replace (.getContentRaw message) #"<@!?(\d+?)>" "\"$1\"")]
    (when (.startsWith translated-message ",")
      (let [output (StringWriter.)]
        (safe-eval (safe-read (.substring translated-message 1)) output)
        (.toString output)))))

(defn message-handler [jda owner]
  (let [safe-eval (make-safe-eval jda owner)]
    (fn [message]
      (handle-message safe-eval message))))

(defn event-listener [jda]
  (let [handler (message-handler jda false)
        owner-handler (message-handler jda true)]
    (proxy [ListenerAdapter] []

      (onReady [event]
        (let [guild (-> jda (.getGuilds) (.get 0))
              modc (-> guild (.getController))]
          (intern 'owner-sandbox 'guild guild)
          (intern 'owner-sandbox 'modc modc))
        (println "Bot ready!")
        (-> jda (.getTextChannelById 444366153683304458) (.sendMessage "REPL activated") (.queue)))

      (onMessageReceived [event]
        (let [owner (= (-> event
                           (.getAuthor)
                           (.getId))
                       "118330468025237505")
              use-handler (if owner owner-handler handler)]
          (let [message (.getMessage event)]
            (when owner (intern 'owner-sandbox 'message message))
            (let [response (use-handler message)]
              (when response
                (-> message
                    (.getChannel)
                    (.sendMessage response)
                    (.queue))))))))))

(defn connect
  [token]
  (let [jda (-> AccountType/BOT
                JDABuilder.
                (.setToken token)
                .buildAsync)]
    (.addEventListener jda (object-array [(event-listener jda)]))))

(defn -main
  []
  (connect (.trim (slurp "token.txt"))))
