(ns owner-sandbox)

(defn member [guild id]
  (-> guild (.getMemberById id)))

(defn reload-repl [] (require 'kaddy-clj.core :reload))
